
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2021.04

using System;
using System.Runtime.InteropServices;
using UnityEngine;

public class GameplayController : MonoBehaviour
{
    #if UNITY_IPHONE
        const string DllName = "__Internal";
    #else
        const string DllName = "DLLExample";
    #endif

    private const int TRUE  = 1;
    private const int FALSE = 0;

    private UIntPtr unmanagedInstance;

    public GameplayController()
    {
        unmanagedInstance = gameplay_controller_factory (0);
    }

    public void RestartBoard ()
    {
        gameplay_controller_restart_board (unmanagedInstance);
    }

    public bool ValueIsOk (int col, int row)
    {
        var ok = gameplay_controller_value_is_ok (unmanagedInstance, col, row);

        if (ok < 0)
        {
            throw new ArgumentException();
        }

        return ok == TRUE ? true : false;
    }

    public void ClearCellValue (int col, int row)
    {
        if (gameplay_controller_clear_cell_value (unmanagedInstance, col, row) != TRUE)
        {
            throw new ArgumentException();
        }
    }

    public void SetCellValue (int col, int row, int newValue)
    {
        if (gameplay_controller_set_cell_value (unmanagedInstance, col, row, newValue) != TRUE)
        {
            throw new ArgumentException();
        }
    }

    public void Start ()
    {
        SetCellValue (1, 2, 3);

        print ("Cell value = " + GameData.board.GetCellValue (1, 2));
    }

    public void Update ()
    {
    }

    [DllImport(DllName)] private static extern UIntPtr gameplay_controller_factory          (int difficulty);
    [DllImport(DllName)] private static extern void    gameplay_controller_restart_board    (UIntPtr instance);
    [DllImport(DllName)] private static extern int     gameplay_controller_value_is_ok      (UIntPtr instance, int col, int row);
    [DllImport(DllName)] private static extern int     gameplay_controller_clear_cell_value (UIntPtr instance, int col, int row);
    [DllImport(DllName)] private static extern int     gameplay_controller_set_cell_value   (UIntPtr instance, int col, int row, int new_value);

}
