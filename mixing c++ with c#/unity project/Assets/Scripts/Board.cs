
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2021.04

using System;
using System.Runtime.InteropServices;

public class Board
{
    #if UNITY_IPHONE
        const string DllName = "__Internal";
    #else
        const string DllName = "DLLExample";
    #endif

    private UIntPtr unmanagedInstance;

    public Board()
    {
        unmanagedInstance = board_factory ();
    }

    public Board(UIntPtr existing_instance)
    {
        unmanagedInstance = existing_instance;
    }

    public static uint GetBoardSize ()
    {
        return board_get_board_size ();
    }

    public void ClearBoard ()
    {
        board_clear (unmanagedInstance);
    }

    public int GetCellValue (int col, int row)
    {
        var value = board_get_cell_value (unmanagedInstance, col, row);

        if (value < 0)
        {
            throw new ArgumentException();
        }

        return value;
    }

    [DllImport(DllName)] private static extern UIntPtr board_factory        ();
    [DllImport(DllName)] private static extern uint    board_get_board_size ();
    [DllImport(DllName)] private static extern void    board_clear          (UIntPtr instance);
    [DllImport(DllName)] private static extern int     board_get_cell_value (UIntPtr instance, int col, int row);

}
