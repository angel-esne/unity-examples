
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2021.04

using System;
using System.Runtime.InteropServices;

public static class GameData
{
    #if UNITY_IPHONE
        const string DllName = "__Internal";
    #else
        const string DllName = "DLLExample";
    #endif

    public static Board board { get; }

    static GameData()
    {
        board = new Board(game_data_get_board ());
    }

    [DllImport(DllName)] private static extern UIntPtr game_data_get_board ();

}
