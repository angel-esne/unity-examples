
// Este c�digo es de dominio p�blico.
// angel.rodriguez@esne.edu
// 2021.04

#pragma once

#include "Game_Data.hpp"
#include "Gameplay_Controller.hpp"

#define EXPORTED_FUNCTION __declspec(dllexport)

extern "C"
{

    EXPORTED_FUNCTION void *   board_factory        ();
    EXPORTED_FUNCTION unsigned board_get_board_size ();
    EXPORTED_FUNCTION void     board_clear          (void * instance);
    EXPORTED_FUNCTION int      board_get_cell_value (void * instance, int col, int row);

    EXPORTED_FUNCTION void *   game_data_get_board  ();

    EXPORTED_FUNCTION void *   gameplay_controller_factory          (int  difficulty);
    EXPORTED_FUNCTION void     gameplay_controller_restart_board    (void * instance);
    EXPORTED_FUNCTION int      gameplay_controller_value_is_ok      (void * instance, int col, int row);
    EXPORTED_FUNCTION int      gameplay_controller_clear_cell_value (void * instance, int col, int row);
    EXPORTED_FUNCTION int      gameplay_controller_set_cell_value   (void * instance, int col, int row, int new_value);

}
