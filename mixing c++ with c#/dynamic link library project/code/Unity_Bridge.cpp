
// Este c�digo es de dominio p�blico.
// angel.rodriguez@esne.edu
// 2021.04

#include <list>
#include <memory>
#include "Unity_Bridge.hpp"

using namespace std;
using namespace model;
using namespace controller;

namespace
{

    struct
    {
        list< unique_ptr< Board               > > boards;
        list< unique_ptr< Gameplay_Controller > > gameplay_controllers;
    }
    memory_handler;

}

extern "C"
{

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // BOARD

    void * board_factory ()
    {
        auto board = new Board;

        if (board != nullptr)
        {
            memory_handler.boards.emplace_back (board);
        }

        return board;
    }

    unsigned board_get_board_size ()
    {
        return Board::get_board_size ();
    }

    void board_clear (void * instance)
    {
        if (instance)
        {
            auto board = reinterpret_cast< Board * >(instance);

            board->clear ();
        }
    }

    int32_t board_get_cell_value (void * instance, int col, int row)
    {
        if (instance)
        {
            auto board = reinterpret_cast< Board * >(instance);

            return int32_t( board->get_cell_value (col, row) );
        }

        return -1;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // GAME DATA

    void * game_data_get_board ()
    {
        return &model::Game_Data::instance ().board;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // GAMEPLAY CONTROLLER

    void * gameplay_controller_factory (int difficulty)
    {
        auto gameplay_controller = new Gameplay_Controller(difficulty);

        if (gameplay_controller != nullptr)
        {
            memory_handler.gameplay_controllers.emplace_back (gameplay_controller);
        }

        return gameplay_controller;
    }

    void gameplay_controller_restart_board (void * instance)
    {
        if (instance)
        {
            auto gameplay_controller = reinterpret_cast< Gameplay_Controller * >(instance);

            gameplay_controller->restart_board ();
        }
    }
    
    int gameplay_controller_value_is_ok (void * instance, int col, int row)
    {
        if (instance)
        {
            auto gameplay_controller = reinterpret_cast< Gameplay_Controller * >(instance);

            return gameplay_controller->value_is_ok (col, row);
        }

        return false;
    }
    
    int gameplay_controller_clear_cell_value (void * instance, int col, int row)
    {
        if (instance)
        {
            auto gameplay_controller = reinterpret_cast< Gameplay_Controller * >(instance);

            return gameplay_controller->clear_cell_value (col, row);
        }

        return false;
    }

    int gameplay_controller_set_cell_value (void * instance, int col, int row, int new_value)
    {
        if (instance)
        {
            auto gameplay_controller = reinterpret_cast< Gameplay_Controller * >(instance);

            return gameplay_controller->set_cell_value (col, row, new_value);
        }

        return false;
    }

}
