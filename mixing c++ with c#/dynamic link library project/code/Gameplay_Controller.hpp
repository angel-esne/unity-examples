
// Este c�digo es de dominio p�blico.
// angel.rodriguez@esne.edu
// 2021.04

#pragma once

#include "Game_Data.hpp"

namespace controller
{

    using model::Board;

    class Gameplay_Controller
    {
    private:

        int     difficulty_level;
        Board & board;

    public:

        Gameplay_Controller(int given_difficulty_level = 0)
        :
            board(model::Game_Data::instance ().board)
        {
            difficulty_level = given_difficulty_level;
        }

        void restart_board ()
        {
            // TO DO
        }

        bool clear_cell_value (int col, int row)
        {
            return board.clear_cell_value (col, row);
        }

        bool set_cell_value (int col, int row, int new_value)
        {
            if (new_value > 0 && unsigned(new_value) < Board::get_board_size ())
            {
                return board.set_cell_value (col, row, new_value);
            }

            return false;
        }

        int value_is_ok (int col, int row)
        {
            // TO DO
            return -10;
        }

    };

}
