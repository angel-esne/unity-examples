
// Este c�digo es de dominio p�blico.
// angel.rodriguez@esne.edu
// 2021.04

#pragma once

#include "Board.hpp"

namespace model
{

    struct Game_Data
    {

        static Game_Data & instance ()
        {
            static Game_Data game_data;
            return game_data;
        }

        Board board;

    };

}
