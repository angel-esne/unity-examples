
// Este c�digo es de dominio p�blico.
// angel.rodriguez@esne.edu
// 2021.04

#pragma once

namespace model
{

    class Board
    {
    private:

        /// Establece el tama�o del tablero, que es siempre cuadrado.
        static constexpr unsigned board_size = 9;

        /// Guarda los valores de las casillas tablero (seg�n las reglas son n�meros en el intervalo
        /// [0, board_size]). El valor 0 indica que la casilla est� vac�a.
        int values[board_size][board_size];

    public:

        /** El constructor deja el tablero vac�o.
          */
        Board()
        {
            clear ();
        }

        /** Retorna el tama�o del tablero, que es siempre cuadrado.
          */ 
        static constexpr unsigned get_board_size ()
        {
            return board_size;
        }

        void clear ()
        {
            for (unsigned row = 0; row < board_size; ++row)
            {
                for (unsigned col = 0; col < board_size; ++col)
                {
                    values[col][row] = 0;
                }
            }
        }

        bool clear_cell_value (int col, int row)
        {
            if (coordinates_within_range (col, row))
            {
                values[col][row] = 0;

                return true;
            }

            return false;
        }

        /** Permite establecer el valor de una casilla.
          */
        bool set_cell_value (int col, int row, int new_value)
        {
            if (coordinates_within_range (col, row))
            {
                values[col][row] = new_value;

                return true;
            }

            return false;
        }

        /** Permite conocer el valor de una casilla.
          */
        int get_cell_value (int col, int row)
        {
            if (coordinates_within_range (col, row))
            {
                return values[col][row];
            }

            return -1;
        }

        bool coordinates_within_range (int col, int row)
        {
            return row > 0 && row < board_size && col > 0 && col < board_size;
        }

    };

}
